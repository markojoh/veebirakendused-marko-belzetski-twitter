from dbConnect import Database
from MySQLdb import escape_string as thwart
import gc


class Tweet(object):
	""" This is a tweet object """

	def __init__(self, tweetID, uid, text, datetime):
		c,conn = Database()

		self.text = text
		self.datetime = datetime
		self.uid = uid

		c.execute("SELECT username FROM users WHERE uid = (%r)", (self.uid))
		
		self.tweetID = tweetID
		self.username = c.fetchone()[0]
		gc.collect()