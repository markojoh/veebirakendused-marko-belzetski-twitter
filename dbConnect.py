# coding=utf-8
import MySQLdb
import mysqlaccess
import gc

# with open('mysqlaccess.txt','r') as file:
# 	mysqlpassword = str(file.readline())

def Database():
	conn = MySQLdb.connect(host="localhost",user = "root",passwd = mysqlaccess.password,db = "markoTwitter",use_unicode=True,charset="utf8") 
	c = conn.cursor()
	return c, conn


def delete_tweet(tweetID):
	c,conn = Database()
	c.execute("DELETE FROM tweets WHERE tweetID=%r", (tweetID))
	conn.commit()
	gc.collect()


def get_uid(user):
	c,conn = Database()
	c.execute("SELECT uid FROM users WHERE username=%s", (user))
	uid = int(c.fetchone()[0])
	gc.collect()
	return uid