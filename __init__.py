from flask import Flask, render_template, url_for,request,session,redirect,request,flash
from dbConnect import Database, delete_tweet, get_uid
from MySQLdb import escape_string as thwart
from passlib.hash import sha256_crypt
from functools import wraps
import gc
from tweet import Tweet
import re

app = Flask(__name__)

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('homepage'))

    return wrap

def chrome_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if request.user_agent.browser == "chrome":
        	return f(*args, **kwargs)
        else:
            # flash("You need to be logged in as developer to access this feature.")
            return "You need to be using chrome to access this page"
    return wrap

def get_tweets():
	c,conn = Database()
	c.execute("SELECT * FROM tweets")
	rows = c.fetchall()
	tweets = [Tweet(row[0],row[1],row[2],row[3]) for row in rows]
	gc.collect()
	return tweets

def get_user_tweets(uid):
	c,conn = Database()
	c.execute("SELECT * FROM tweets WHERE uid=%r", (uid))
	rows = c.fetchall()
	tweets = [Tweet(row[0],row[1],row[2],row[3]) for row in rows]
	gc.collect()
	return tweets

def redirect_url(default='index'):
    return request.args.get('next') or \
           request.referrer or \
           url_for(default)

def login(request):
	c, conn = Database()
	username = str(request.form['loginUsername'])
	try:
		data = c.execute("SELECT * FROM users WHERE username = (%s)", (thwart(username)))
		user = c.fetchone()
		pw = str(user[2])
		uid = user[0]

		if sha256_crypt.verify(request.form['loginPassword'], pw):
			session['logged_in'] = True
			session['username'] = username
			session['uid'] = uid
			
		else:
			flash("Authentication error. Please try again.")

	except Exception as e:
		error = e
		flash("Authentication error. Please try again.")

	conn.commit()
	gc.collect()
	return redirect(redirect_url())


def register(request):
	c, conn = Database()
	username = str(request.form['registerUsername'])
	if len(username) < 1:
		flash("Username must be at least 1 character long")
	email = str(request.form['registerEmail'])

	if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
		flash("You must enter a valid e-mail address")

	if len(request.form['registerPassword']) < 8:
		flash("Your password must be at least 8 characters long.")

	password = sha256_crypt.encrypt(str(request.form['registerPassword']))

	if not(len(username) < 1 or not re.match(r"[^@]+@[^@]+\.[^@]+", email) or len(request.form['registerPassword'])< 8):
		try:
			c.execute("INSERT INTO users VALUES(0,%s,%s,%s)", (username,password,email))
			flash ("User %s successfully created!" % username)
		except Exception,e:
			flash(e)
			error = e

	conn.commit()
	gc.collect()
	return redirect(redirect_url())


def tweet(request):
	c, conn = Database()
	tweet = str(request.form['tweetText'])
	if len(tweet) < 1:
		flash("Please enter at least one character")
	else:
		c.execute("INSERT INTO tweets VALUES(0,%r,%s,NOW())", (session['uid'],tweet))
	conn.commit()
	gc.collect()
	return redirect(redirect_url())

def edit(request):
	c, conn = Database()
	tweet = str(request.form['editText'])
	if len(tweet) < 1:
		flash("Please enter at least one character")
	else:
		tweetID = int(request.form['editid'])
		c.execute("UPDATE tweets SET text=%s WHERE tweetID=%r", (tweet,tweetID))
	conn.commit()
	gc.collect()
	return redirect(redirect_url())

@app.route('/', methods=["GET","POST"])
def homepage():
	pT = "Main page"
	error = ""
	# Since we have three forms on the main page (one for logging in, the other for registering, last for tweeting),
	# we need to differentiate between the three
	if request.method == "POST":
		#parse input from loginform
		if request.form['submit'] == "login":
			login(request)
		#parse input from registering form:
		elif request.form['submit'] == "register":
			register(request)
		#send tweet
		elif request.form['submit'] == "tweet":
			tweet(request)
		#edit tweet
		elif request.form['submit'] == "edit":
			edit(request)
	return render_template("home.html", pageTitle = pT, err = error,tweets=get_tweets())

@app.route("/logout/")
@login_required
def logout():
    session.clear()
    gc.collect()
    return redirect(redirect_url())

@app.route('/u/<user>', methods=["GET","POST"])
def user(user):
	pT= str(user)
	error = ''
	uid = get_uid(user)

	if request.method == "POST":
		#parse input from loginform
		if request.form['submit'] == "login":
			login(request)
		#parse input from registering form:
		elif request.form['submit'] == "register":
			register(request)
		#edit tweet
		elif request.form['submit'] == "edit":
			edit(request)
	return render_template("user.html", pageTitle = pT, err = error,tweets=get_user_tweets(uid), user=user)

@app.route('/delete/<deleteid>')
def delete(deleteid):
	delete_tweet(int(deleteid))
	return redirect(redirect_url())

@app.route('/browser/')
def browser():
	return request.user_agent.browser  

if __name__ == "__main__":
    app.run()