from tweet import Tweet
from dbConnect import Database
from MySQLdb import escape_string as thwart
import gc

class User(object):
	""" This is a user object """

	def __init__(self, uid, name, email):
		self.uid = uid
		self.name = name
		self.email = email

	def getTweets(self):
		""" This function gets all the tweets for this user and returns a list of tweets"""
		tweets = []
		c,conn = Database()
		c.excecute("SELECT * FROM tweets WHERE uid=%r", (self.uid))
		tweetsRows = c.fetchall()
		for row in tweetsRows:
			tweetID, uid, text, datetime = row
			tweets.append(Tweet(id,uid,text,datetime))
		gc.collect()
		return tweets

	
