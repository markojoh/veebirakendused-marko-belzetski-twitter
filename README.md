# README #

Rakenduse leiab aadressilt http://146.185.145.30/

### Veebirakenduste aine projekt - lihtne Twitteri kloon ###

Rakendus on üles ehitatud Flask (http://flask.pocoo.org/) backend ja Bootstrap (https://getbootstrap.com/) frontend raamistikele.

### Kirjeldus ##

Tegemist on väga lihtsakoelise Twitteri klooniga. Lehe külalised näevad kõikide teiste poolt kirjutatud tweete. Ise tweetimiseks tuleb luua kasutajakonto.

Kõik Tweetid ja Kasutajate andmed on hoitud MYSQL andmebaasis.

### MYSQL andmebaasi ülesehitus ###

Databse: markoTwitter


```
#!MYSQL

Table:users

+----------+------------------+------+-----+---------+----------------+
| Field    | Type             | Null | Key | Default | Extra          |
+----------+------------------+------+-----+---------+----------------+
| uid      | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
| username | varchar(20)      | NO   | UNI | NULL    |                |
| password | varchar(100)     | NO   |     | NULL    |                |
| email    | varchar(50)      | NO   |     | NULL    |                |
+----------+------------------+------+-----+---------+----------------+

```





```
#!mysql

Table: tweets

+----------+------------------+------+-----+---------+----------------+
| Field    | Type             | Null | Key | Default | Extra          |
+----------+------------------+------+-----+---------+----------------+
| tweetID  | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
| uid      | int(10) unsigned | NO   | MUL | NULL    |                |
| text     | varchar(160)     | NO   |     | NULL    |                |
| datetime | datetime         | NO   |     | NULL    |                |
+----------+------------------+------+-----+---------+----------------+
```



### Dependancyd ###

apache2 mysql-client mysql-server libapache2-mod-wsgi virtualenv python-pip Flask


### Projekti autor ###

* Marko Belzetski
* Infosüsteemide arendus
* Rühm 14