function edit(tweetid)
{
	var tweetbox = document.getElementById(tweetid);
	var tweettext = tweetbox.textContent.trim();
	var buttonsID = tweetid+'buttons'
	var buttons = document.getElementById(buttonsID);
	buttons.style.display = 'none';
	tweetbox.innerHTML = '<form method="post" action="">' + 
  	'<div class="form-group">' + 
    '<textarea class="form-control" id="editText" name="editText" value="" minlength="1" maxlength="160"></textarea>' + 
  	'</div>' +
  	'<input type="hidden" id="editid" name="editid" value="'+tweetid+'">' + 
  	'<button type="submit" name="submit" id="submit" value="edit" class="btn btn-default">Save</button>' +
	'</form>'
	document.getElementById('editText').value = tweettext;
}